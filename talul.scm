(include "adjectives")
(include "prefixes")
(include "nouns")
(include "verbs")

(use data-structures)
(use irregex)
(use srfi-13)


(define parse
  (lambda (input)
    (print (list 
     (irregex-replace "- " (string-intersperse (flatten (map translate-word input))) "-")))))


(define translate-word
  (lambda (word)
    (cond ((list? (assoc (string-titlecase word) nouns))
           (cdr (assoc (string-titlecase word) nouns)))
          ((list? (assoc (string-titlecase word) adjectives))
           (cdr (assoc (string-titlecase word) adjectives)))
          ((list? (assoc (string-titlecase word) verbs))
           (cdr (assoc (string-titlecase word) verbs)))
          ((list? (assoc (string-titlecase word) prefixes))
           (cdr (assoc (string-titlecase word) prefixes)))
          (else word))))

(print (map string-titlecase (command-line-arguments)))
(parse (string-split (string-intersperse (command-line-arguments)) "- "))

(exit)

