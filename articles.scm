(define articles '( "the"
                    "a" "an"
                    "this" "that"
                    "my" "yours" "his" "hers" "its" "our" "theirs" "whose"
                    "which" "what"
                    "whichever" "whatever"))
